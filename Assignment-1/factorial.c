#include<stdio.h>
#include<math.h>

int main()
{
  int t, n, x, f, i, digsize=0, temp;
  int dig[160]={0};
  
	scanf("%d", &t);

	for(x = 0; x<t; x++)
	  {
	    scanf("%d", &n);

	    for(i=0; i<160; i++)
	      dig[i]=0;

	    dig[0]=1;
	    digsize=1;
	    
	    for(i=2; i<=n; i++)
	      {
		temp = 0;
		
		for(f=0; f<digsize; f++)
		  {
		    temp = dig[f]*i + temp;
		    
		    dig[f] = temp%10;
		    temp = temp/10;
     
		  }
		for(;temp!=0;)
		  {
		    dig[digsize] = temp%10;
		    digsize = digsize + 1;
		    temp = temp/10;
	     
		  }
	      }

	    for(i=digsize-1; i>=0; i--)
	      printf("%d", dig[i]);
	    printf("\n");
	  }

	return 0;
}   
