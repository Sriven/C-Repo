#include<stdio.h>
#include<math.h>

#define MAX 1000001
unsigned long long int sum[MAX];

int main()
{
	
	int k;
	for(k = 2; k < MAX; k++)
		sum[k] = k;

	
	for (k = 2; k*k <= MAX; k++)
	{
		if (sum[k]!=0)
		{
			int f;
			for(f = k*2; f<MAX; f += k)
				sum[f]=0;
		}
	}
	
	for(k = 3; k < MAX; k++)
		sum[k] += sum[k-1];

	int t;
	scanf("%d", &t);

	while(t--)
	{
		int n, m;
		scanf("%d %d", &n, &m);
		printf("%llu\n" , (sum[m]-sum[n-1]));
	}
	return 0;
}


