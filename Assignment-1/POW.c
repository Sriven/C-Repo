#include<stdio.h>
#include<math.h>

int main()
{
	unsigned long long int t, i, j, k, x=0, y=0;

	scanf("%llu", &t);

	for(x=0; x<t; x++)
	{
		scanf("%llu %llu %llu", &i, &j, &k);
		if (j == 0) 
		{
			printf("1");
			continue;
		}
    		
		unsigned long long int a = 1;
    		
		for(;j > 1;) 
		{
		        if (j % 2 != 0) 
			{
		            a = a*i;
		            a = a%k;
		        }
		        
			i = i*i;
		        i = i%k;
		        j = j/2;
    		}
    			y = (i*a) % k;
			printf("%llu\n", y);
	
	}

	return 0;
}
